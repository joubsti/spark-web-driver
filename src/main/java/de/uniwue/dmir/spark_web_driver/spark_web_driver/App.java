package de.uniwue.dmir.spark_web_driver.spark_web_driver;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SQLContext;

public class App 
{
	private static SparkConf conf;
	private static JavaSparkContext sc;
	
	public static void main(String[] args) {

    	String parquetPath = "";
    	String master = "";
    	String appName = "";
    	String jarPath = "";
    	int port = 0;
    	
	    for(int i = 0; i < args.length; i++){
	    	String arg = args[i];
	    	
	    	if( arg.equals("--help") || arg.equals("-help") ){
	    		System.out.println("This is the help context\n");
	    		System.out.println("Available options:");
	    		System.out.println("\t--port <port>\t: specifies the listening port");
	    		System.out.println("\t--parquet <path>\t: specifies the parquet path");
	    		System.out.println("\t--master <master>\t: specifies the master endpoint");
	    		System.out.println("\t--appname <name>\t: specifies the application name");
	    		System.out.println("\t--jarpath <path>\t: specifies the jar archive path");
	    		return;
	    	} else if(arg.equals("--port") ){
	    		if( args.length < i+1){
	    			System.out.println("You must specify the listen port");
	    			return;
	    		}
	    		arg = args[++i];
	    		try{
	    			port = Integer.parseInt(arg);
	    		} catch(NumberFormatException x){
	    			System.out.println("the given listen port ist not a number");
	    			return;
	    		}
	    	} else if(arg.equals("--parquet") ){
	    		if( args.length < i+1){
	    			System.out.println("You must specify the parquet path");
	    			return;
	    		}
	    		arg = args[++i];
	    		parquetPath = arg;
	    	} else if(arg.equals("--master") ){
	    		if( args.length < i+1){
	    			System.out.println("You must specify the master");
	    			return;
	    		}
	    		arg = args[++i];
	    		master = arg;
	    	} else if(arg.equals("--appname") ){
	    		if( args.length < i+1){
	    			System.out.println("You must specify the application name");
	    			return;
	    		}
	    		arg = args[++i];
	    		appName = arg;
	    	} else if(arg.equals("--jarpath") ){
	    		if( args.length < i+1){
	    			System.out.println("You must specify the jar path");
	    			return;
	    		}
	    		arg = args[++i];
	    		jarPath = arg;
	    	}
	    }
	    
	    
	    if( port == 0 ){
	    	System.out.println("You must specify the listen port using --port <port>");
	    	return;
	    }
	    
	    if( master == "" ){
	    	System.out.println("You must specify the master using --master <master>");
	    	return;
	    }
	    
	    if( parquetPath == "" ){
	    	System.out.println("You must specify the parquet path using --parquet <path>");
	    	return;
	    }
	    
	    if( appName == "" ){
	    	System.out.println("You must specify the application name using --appname <name>");
	    	return;
	    }
	    
	    if( jarPath == "" ){
	    	System.out.println("You must specify the jar path using --jarpath <path>");
	    	return;
	    }
	    
        conf = new SparkConf().setAppName(appName).setMaster(master);
        conf.setJars(new String[]{jarPath});
        sc = new JavaSparkContext(conf);
        SQLContext sqlc = new SQLContext(sc);
        de.uniwue.dmir.spark_web_driver.spark_web_driver_core.App.startByShell(parquetPath, appName, port, JavaSparkContext.toSparkContext(sc), sqlc);
    }
}
